/* $*************** KCG Version 6.1.3 (build i6) ****************
** Command: s2c613 -config C:/Program Files (x86)/Esterel Technologies/Esterel SCADE 6.4.2i25/examples/CruiseControl/CruiseControl/KCG\kcg_s2c_config.txt
** Generation date: 2013-06-12T22:26:25
*************************************************************$ */
#ifndef _KCG_CONSTS_H_
#define _KCG_CONSTS_H_

#include "kcg_types.h"

/* CruiseControl::PedalsMin */
#define PedalsMin_CruiseControl 3.0

/* CruiseControl::Kp */
#define Kp_CruiseControl 8.113

/* CruiseControl::Ki */
#define Ki_CruiseControl 0.5

/* CruiseControl::ZeroSpeed */
#define ZeroSpeed_CruiseControl 0.0

/* CruiseControl::ZeroPercent */
#define ZeroPercent_CruiseControl 0.0

/* CruiseControl::RegulThrottleMax */
#define RegulThrottleMax_CruiseControl 45.0

/* CruiseControl::SpeedMax */
#define SpeedMax_CruiseControl 150.0

/* CruiseControl::SpeedMin */
#define SpeedMin_CruiseControl 30.0

/* CruiseControl::SpeedInc */
#define SpeedInc_CruiseControl 2.5

#endif /* _KCG_CONSTS_H_ */
/* $*************** KCG Version 6.1.3 (build i6) ****************
** kcg_consts.h
** Generation date: 2013-06-12T22:26:25
*************************************************************$ */

