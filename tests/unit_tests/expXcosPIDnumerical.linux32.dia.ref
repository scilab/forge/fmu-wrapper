//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) - 2014 - Scilab Enterprises - Vladislav Trubkin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- XCOS TEST -->
// Test of numerical results of Xcos PID controller. Comparison with FMU checker
// results.
cwd = pwd();
assert_checktrue(importXcosDiagram(cwd + "\tests\unit_tests\expXcosPIDnumerical.zcos"));
xcosSolver = ["***Lsodar***"
              "***CVode BDF Newton***"
              "***CVode BDF Functional***"
              "***CVode ADAMS Newton***"
              "***CVode ADAMS Functional***"
              "***Dormand-Prince***"
              "***Runge-Kutta***"];
// load comparison function
exec(cwd + "\tests\unit_tests\check_results.sce", -1);
// read FMU checker results
outME = csvRead(cwd + "\tests\unit_tests\expXcosPIDMEnumericalFMUcheckOut.csv", ";");
outCS = csvRead(cwd + "\tests\unit_tests\expXcosPIDCSnumericalFMUcheckOut.csv", ";");
// delete headers
outME = outME(3:$,:);
outCS = outCS(2:$,:);
// FMUchecker time
assert_checkequal(outME(:,1), outCS(:,1));
outputs = struct();
outputs.time = outME(:,1);
// FMUchecker values
outputs.values = [outME(:,2), outCS(:,2)];
// compare Xcos superblock results with FMUchecker results
// PIDrefs == Xcos superblock results
// outputs == FMUchecker results for Xcos FMUs 
disp("//*********Xcos superblock and FMUchecker*********//");
 
 //*********Xcos superblock and FMUchecker*********//   
outName = ["super_MEFMUchecker"; "super_CSFMUchecker"];
for i=1:7
    scs_m.props.tol(6) = i-1;
    xcos_simulate(scs_m, 4);
    check_results(PIDrefs, outputs, outName, xcosSolver(i));
end
 
 Type of solver: ***Lsodar***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008521   0.0051648           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000787   0.0004763           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***CVode BDF Newton***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008637   0.0052245           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000904   0.0005381           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***CVode BDF Functional***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008579   0.0051951           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000845   0.0005068           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***CVode ADAMS Newton***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008546   0.0051721           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000812   0.0004838           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***CVode ADAMS Functional***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008561   0.0051863           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000827   0.0004977           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***Dormand-Prince***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008497   0.0051596           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000763   0.0004713           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***Runge-Kutta***   
 
 
         column 1 to 5
 
!                    Mean error  Standard deviation  Min  Max        !
!                                                                    !
!super_MEFMUchecker  0.0008496   0.0051596           0    1.1365228  !
!                                                                    !
!super_CSFMUchecker  0.0000762   0.0004712           0    1.0956953  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
// compare FMUchecker results with Xcos import FMU
disp("//*********FMUchecker and Xcos import FMU*********//");
 
 //*********FMUchecker and Xcos import FMU*********//   
outName = ["xcosME_MEFMUchecker"; "xcosCS_CSFMUchecker"];
outRefs = struct();
outRefs.time = outputs.time;
// ME FMUchecker
outRefs.values = outputs.values(:,1);
for i=1:7
    scs_m.props.tol(6) = i-1;
    xcos_simulate(scs_m, 4);
    check_results(outRefs, PIDresults, outName(1), xcosSolver(i));
end
 
 Type of solver: ***Lsodar***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0008899   0.0053988           0    1.0901578  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***CVode BDF Newton***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0009834   0.0062783           0    1.0878167  !
 
         column 6
 
!Order      !
!           !
!1.0002593  !
 
 Type of solver: ***CVode BDF Functional***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0008567   0.0051933           0    1.0914866  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***CVode ADAMS Newton***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0009130   0.0055761           0    1.0891164  !
 
         column 6
 
!Order      !
!           !
!1.0001282  !
 
 Type of solver: ***CVode ADAMS Functional***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0008544   0.0051820           0    1.0916333  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***Dormand-Prince***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0007301   0.0044540           0    1.1003015  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***Runge-Kutta***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosME_MEFMUchecker  0.0007652   0.0046535           0    1.0977626  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
// CS FMUchecker
outRefs.values = outputs.values(:,1);
for i=1:7
    scs_m.props.tol(6) = i-1;
    xcos_simulate(scs_m, 4);
    check_results(outRefs, PIDresults, outName(2), xcosSolver(i));
end
 
 Type of solver: ***Lsodar***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0008899   0.0053988           0    1.0901578  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***CVode BDF Newton***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0009834   0.0062783           0    1.0878167  !
 
         column 6
 
!Order      !
!           !
!1.0002593  !
 
 Type of solver: ***CVode BDF Functional***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0008567   0.0051933           0    1.0914866  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***CVode ADAMS Newton***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0009130   0.0055761           0    1.0891164  !
 
         column 6
 
!Order      !
!           !
!1.0001282  !
 
 Type of solver: ***CVode ADAMS Functional***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0008544   0.0051820           0    1.0916333  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***Dormand-Prince***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0007301   0.0044540           0    1.1003015  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
 
 Type of solver: ***Runge-Kutta***   
 
 
         column 1 to 5
 
!                     Mean error  Standard deviation  Min  Max        !
!                                                                     !
!xcosCS_CSFMUchecker  0.0007652   0.0046535           0    1.0977626  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
// compare Xcos superblock results with Xcos import FMU
disp("//*********Xcos superblock and Xcos import FMU*********//");
 
 //*********Xcos superblock and Xcos import FMU*********//   
outName = ["xcosME_superXcos"; "xcosCS_superXcos"];
for i=1:7
    scs_m.props.tol(6) = i-1;
    xcos_simulate(scs_m, 4);
    check_results(PIDrefs, PIDresults, outName, xcosSolver(i));
end
 
 Type of solver: ***Lsodar***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0000716   0.0007240           0    1.0901578  !
!                                                                  !
!xcosCS_superXcos  0.0018150   0.0132456           0    1.0931701  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002629  !
 
 Type of solver: ***CVode BDF Newton***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0004368   0.0035079           0    1.0878167  !
!                                                                  !
!xcosCS_superXcos  0.0009480   0.0064073           0    1.0929463  !
 
         column 6
 
!Order      !
!           !
!1.0002593  !
!           !
!1.0002641  !
 
 Type of solver: ***CVode BDF Functional***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0000012   0.0000061           0    1.0914866  !
!                                                                  !
!xcosCS_superXcos  0.0035599   0.0289907           0    1.0924273  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002639  !
 
 Type of solver: ***CVode ADAMS Newton***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0002598   0.0019308           0    1.0891164  !
!                                                                  !
!xcosCS_superXcos  0.0011875   0.0081963           0    1.0934516  !
 
         column 6
 
!Order      !
!           !
!1.0001282  !
!           !
!1.0002641  !
 
 Type of solver: ***CVode ADAMS Functional***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0000018   0.0000085           0    1.0916333  !
!                                                                  !
!xcosCS_superXcos  0.0038090   0.0318054           0    1.0926504  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002639  !
 
 Type of solver: ***Dormand-Prince***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0001608   0.0010630           0    1.1003015  !
!                                                                  !
!xcosCS_superXcos  0.0000228   0.0001431           0    1.0930657  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
 
 Type of solver: ***Runge-Kutta***   
 
 
         column 1 to 5
 
!                  Mean error  Standard deviation  Min  Max        !
!                                                                  !
!xcosME_superXcos  0.0001123   0.0007432           0    1.0977626  !
!                                                                  !
!xcosCS_superXcos  0.0000379   0.0002344           0    1.0937854  !
 
         column 6
 
!Order      !
!           !
!1.0002644  !
!           !
!1.0002644  !
