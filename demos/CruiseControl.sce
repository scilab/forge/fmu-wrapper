//
// This file is released under the 3-clause BSD license. See COPYING-BSD.
//

demoPath = get_absolute_file_path("CruiseControl.sce");
xcos(demoPath+filesep()+"CruiseControl.zcos");
