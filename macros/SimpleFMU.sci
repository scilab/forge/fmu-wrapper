//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) - 2020 - ESI Group - Clement David
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
function [x, y, typ] = SimpleFMU(job,arg1,arg2)
    x = []; y = []; typ = [];
    select job
    case 'set' then
        x = arg1;
        model = x.model;
        exprs = x.graphics.exprs;

        if size(exprs, '*') < 2 then
            // a new load has to be done
            fname = uigetfile(["*.fmu"], "", "Load FMU for Simulation");
            [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(fname, exprs, [], []);
        else
            filepath = resolve_fmu_filepath(exprs(1));
            // resolve the FMU implementation if previously set
            if size(exprs, '*') >= 3 then
                fmuImpl = exprs(3);
            else
                fmuImpl = [];
            end

            if isdir(exprs(2)) then
                // it looks already or partially loaded, get the information back
                [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(filepath, exprs, exprs(2), fmuImpl);
            elseif isfile(filepath) then
                // a reload has to be done
                [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(filepath, exprs, [], fmuImpl);
            elseif isdir(fileparts(filepath)) then
                // reload the fmu from the same existing directory
                fname = uigetfile(["*.fmu"], fileparts(filepath), "Load FMU for Simulation");
                [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(fname, exprs, [], fmuImpl);
            else
                // none of the previously stored information looks correct, a new load has to be done
                fname = uigetfile(["*.fmu"], "", "Load FMU for Simulation");
                [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(fname, exprs, [], fmuImpl);
            end
        end

        if xml == [] then
            return;
        end

        // default parameters are empty
        realRefs = [];
        realNegatedAlias = [];
        intRefs = [];
        intNegatedAlias = [];
        boolRefs = [];
        boolNegatedAlias = [];

        ok = %f;
        while ~ok
            modelName = xmlXPath(xml, "//@modelName")(1).content;
            desc = "Configuration of " + modelName
            
            // first ask for the common parameters
            labels = ["FMU file" ; "FMU directory" ; "FMU implementation" ; "Enable logging ?" ; "Relative tolerance" ; "Integer as Real" ; "Boolean as Real" ; "String maximum length" ; "Enumeration as Real"]
            typ = list("str", 1, "str", 1, "str", 1, "str", -1, "mat", [1 1], "str", -1, "str", -1, "mat", [1 1], "str", -1);
            ini = exprs(1:9);
            [ok, fmuFile, fmuWorkdir, fmuImpl, loggingOn, relativeTolerance, integerToDouble, booleanToDouble, stringLength, enumToDouble, next] = scicos_getvalue(desc, labels, typ, ini);
            if ~ok then
                return;
            end
            
            // reload in case of FMU parameters changed, will display an error messagebox on failure
            if or(ini(1:3) <> next(1:3)) then
                exprs(1:3) = next(1:3);

                filepath = resolve_fmu_filepath(exprs(1));
                [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(filepath, exprs, exprs(2), fmuImpl);
                if xml == [] then
                    return;
                end
            else
                exprs(1:9) = next;
            end

            // set simulation function
            select fmuImpl
            case "me 1.0" then
                model.sim=list("fmu_simulate_me",4);
            case "me 2.0" then
                model.sim=list("fmu2_simulate_me",4);
            case "me 3.0" then
                model.sim=list("fmu3_simulate_me",4);
            case "cs 1.0" then
                model.sim=list("fmu_simulate_cs",4);
            case "cs 2.0" then
                model.sim=list("fmu2_simulate_cs",4);
            case "cs 3.0" then
                model.sim=list("fmu3_simulate_cs",4);
            else
                messagebox(["Invalid FMU version / mode."],"modal","error");
                ok = %f
                return
            end

            opar(13) = int8(evstr(loggingOn));
            opar(16) = relativeTolerance;
            if evstr(integerToDouble) then
                integerToDouble = 1; // double
            else
                integerToDouble = 3; // int32
            end
            if evstr(booleanToDouble) then
                booleanToDouble = 1; // double
            else
                booleanToDouble = 8; // uint8
            end
            if evstr(enumToDouble) then
                enumToDouble = 1; // double
            else
                enumToDouble = 6; // uint32
            end
            
            // second ask for the parameters
            select fmuImpl
            case "me 1.0" then
                xmlParameters = xmlXPath(xml, "//ScalarVariable[@variability = ""parameter""]");
            case "me 2.0" then
                // See Functional Mock-up Interface 2.0.3, Page 51 of 131, table variability \ causality
                //  - causality = parameter (A with default initial)
                //  - initial = exact (C with preset value)
                xmlParameters = xmlXPath(xml, "//ScalarVariable[@causality = ""parameter""]|//ScalarVariable[@initial = ""exact""]");
            case "me 3.0" then
                // See Functional Mock-up Interface 3.0, https://fmi-standard.org/docs/3.0/#HowToTreatTunable "How to treat tunable variables"
                xmlParameters = xmlXPath(xml, "//ModelVariables/*[@causality = ""parameter""]|//ModelVariables/*[@initial = ""exact""]");
            case "cs 1.0" then
                xmlParameters = xmlXPath(xml, "//ScalarVariable[@variability = ""parameter""]");
            case "cs 2.0" then
                // See Functional Mock-up Interface 2.0.3, Page 51 of 131, table variability \ causality
                //  - causality = parameter (A with default initial)
                //  - initial = exact (C with preset value)
                xmlParameters = xmlXPath(xml, "//ScalarVariable[@causality = ""parameter""]|//ScalarVariable[@initial = ""exact""]");
            case "cs 3.0" then
                // See Functional Mock-up Interface 3.0, https://fmi-standard.org/docs/3.0/#HowToTreatTunable "How to treat tunable variables"
                xmlParameters = xmlXPath(xml, "//ModelVariables/*[@causality = ""parameter""]|//ModelVariables/*[@initial = ""exact""]");
            end
            if xmlParameters.size == 0 then
                ok = %t
                break,
            end

            PARAMS_PER_POPUP = 10; // limit the number of entry per popup
            ranges = 1:PARAMS_PER_POPUP:xmlParameters.size;
            ranges(2, :) = ranges+PARAMS_PER_POPUP-1;
            ranges($) = xmlParameters.size;
            for r=ranges
                N = r(2)-r(1)+1
                outString = "[ok, " + strcat("param" + string(1:N), ", ") + " , next]";

                labels = emptystr(N,1);
                typ = list();
                ini = emptystr(N,1);
                for i=1:N
                    node = xmlParameters(r(1) + i - 1);
                    if length(node.children) > 0 then
                        child = node.children(1);
                    else
                        child = node;
                    end

                    if node.attributes.name <> [] then
                        labels(i) = labels(i) + node.attributes.name + " : ";
                    end
                    if child.attributes.declaredType <> [] then
                        labels(i) = labels(i) + "(" + child.attributes.declaredType + ") ";
                    end
                    if child.attributes.min <> [] then
                        labels(i) = labels(i) + "min=" + child.attributes.min + " ";
                    end
                    if child.attributes.max <> [] then
                        labels(i) = labels(i) + "max=" + child.attributes.max + " ";
                    end
                    if node.attributes.description <> [] then
                        labels(i) = "<HTML><BODY>" + labels(i) + "<BR>" + node.attributes.description + ". "
                    end
                    typ($+1) = "str";
                    typ($+1) = -1;

                    if child.attributes.start <> [] then
                        ini(i) = child.attributes.start;
                    else
                        ini(i) = "0";
                    end
                    
                    // each exprs entry is a triplet ["valueReference" ; "type" ; "value"] ; match the preset valueReference and type, set ini value
                    refIdx = find(exprs(10:3:$-2) == node.attributes.valueReference & exprs(11:3:$-1) == child.name, 1);
                    if refIdx <> [] then
                        ini(i) = exprs(9 + 3*refIdx);
                    end
                end

                execstr(outString + " = scicos_getvalue(desc, labels, typ, ini);");
                if ~ok then
                    xmlDelete(xml);
                    return;
                end

                // update parameters and the opar encoded values
                for i=1:size(next, '*')
                    node = xmlParameters(r(1) + i - 1);
                    if node.name == "ScalarVariable" && length(node.children) > 0 then
                        child = node.children(1);
                    else
                        child = node;
                    end

                    // each exprs entry is a triplet ["valueReference" ; "type" ; "value"] ; set them
                    refIdx = find(exprs(10:3:$-2) == node.attributes.valueReference & exprs(11:3:$-1) == child.name);
                    if refIdx == [] then
                        exprs($+1) = node.attributes.valueReference;
                        refIdx = size(exprs, 1);
                        exprs($+1) = child.name;
                        exprs($+1) = next(i);
                    else
                        // update the value
                        exprs(9 + 3*refIdx(1)) = next(i);
                        
                        // buggy blocks, clear multiple entries
                        if size(refIdx, "*") > 1 then
                            toBeRemoved = 9 + 3 * refIdx(2:$);
                            exprs([toBeRemoved toBeRemoved+1 toBeRemoved+2]) = [];
                        end
                    end

                    // References and values update
                    ref = uint32(strtod(node.attributes.valueReference));
                    hasNegatedAlias = uint32(node.attributes.alias == "negatedAlias");
                    select child.name
                    case "Boolean" then
                        boolRefs = [boolRefs ref];
                        boolNegatedAlias = [boolNegatedAlias hasNegatedAlias];
                        execstr(msprintf("opar(9)($+1) = evstr(param%d) ;", i));
                    case "Float64" then
                        realRefs = [realRefs ref];
                        realNegatedAlias = [realNegatedAlias hasNegatedAlias];
                        execstr(msprintf("rpar($+1) = strtod(param%d) ;", i));
                    case "Int32" then
                        intRefs = [intRefs ref];
                        intNegatedAlias = [intNegatedAlias hasNegatedAlias];
                        // FIXME: int32 is not supported on ModelAdapter.cpp / struct ipar in Scilab 2024.1 ; use double for now
                        // FIXME: 
                        // execstr(msprintf("ipar($+1) = int32(strtod(param%d)) ;", i));
                        execstr(msprintf("ipar($+1) = strtod(param%d) ;", i));
                    case "Integer" then
                        intRefs = [intRefs ref];
                        intNegatedAlias = [intNegatedAlias hasNegatedAlias];
                        // FIXME: int32 is not supported on ModelAdapter.cpp / struct ipar in Scilab 2024.1 ; use double for now
                        // FIXME: 
                        // execstr(msprintf("ipar($+1) = int32(strtod(param%d)) ;", i));
                        execstr(msprintf("ipar($+1) = strtod(param%d) ;", i));
                    case "Real" then
                        realRefs = [realRefs ref];
                        realNegatedAlias = [realNegatedAlias hasNegatedAlias];
                        execstr(msprintf("rpar($+1) = strtod(param%d) ;", i));
                    else
                        msg = "unsupported %s type for %s";
                        messagebox(msprintf(msg, child.name, node.attributes.name),"modal","error");
                        ok = %f
                        return
                    end
                end
            end
        end

        // concatenate refs and negatedAlias
        opar(6) = uint32([realRefs realNegatedAlias]);
        opar(7) = uint32([intRefs  intNegatedAlias]);
        opar(8) = uint32([boolRefs boolNegatedAlias]);

        // inputs
        if part(fmuImpl, 4:7) == "3.0" then
            xmlInputs = xmlXPath(xml, "//ModelVariables/*/[@causality = ""input""]");
        else
            xmlInputs = xmlXPath(xml, "//ScalarVariable[@causality=""input""]");
        end
        labelInputs = emptystr(xmlInputs.size, 1);
        refsInputs = uint32(-ones(xmlInputs.size, 1));
        typesInputs = ones(xmlInputs.size, 1);
        modifierInputs = uint32(zeros(xmlInputs.size, 1));
        for i=1:xmlInputs.size
            node = xmlInputs(i);
            if length(node.children) > 0 then
                child = node.children(1);
            else
                child = node;
            end

            labelInputs(i) = node.attributes.name;
            refsInputs(i) = strtod(node.attributes.valueReference);
            select child.name
            case "Boolean" then
                typesInputs(i) = 3; //'boolean'
            case "Enumeration" then
                typesInputs(i) = 5; //'enumeration'
            case "Float64" then
                typesInputs(i) = 1; //'real'
            case "Int32" then
                typesInputs(i) = 2; //'integer'
            case "Integer" then
                typesInputs(i) = 2; //'integer'
            case "Real" then
                typesInputs(i) = 1; //'real'
            case "String" then
                typesInputs(i) = 4; //'string'
            end
            select node.attributes("alias")
            case "alias" then
                // valueReference are the same, do not care
                modifierInputs(i) = 0;
            case "negatedAlias" then
                modifierInputs(i) = 1;
            end
            select node.attributes("variability")
            case "discrete" then
                modifierInputs(i) = modifierInputs(i) + 2;
            end
        end
        model.in = ones(typesInputs);
        model.in2 = ones(typesInputs);
        // set string maximum length
        for i = typesInputs(typesInputs == 4)'
            model.in2(i) = stringLength;
        end
        model.intyp = [1 integerToDouble booleanToDouble 5 enumToDouble](typesInputs);
        opar(2) = [refsInputs ; modifierInputs];
        opar(3) = int32(typesInputs);

        // outputs
        if part(fmuImpl, 4:6) == "3.0" then
            xmlOutputs = xmlXPath(xml, "//ModelVariables/*[@causality = ""output""]");
        else
            xmlOutputs = xmlXPath(xml, "//ScalarVariable[@causality=""output""]");
        end
        labelOutputs = emptystr(xmlOutputs.size, 1);
        refsOutputs = uint32(-ones(xmlOutputs.size, 1));
        typesOutputs = ones(xmlOutputs.size, 1);
        modifierOutputs = uint32(zeros(xmlOutputs.size, 1));
        for i=1:xmlOutputs.size
            node = xmlOutputs(i);

            labelOutputs(i) = node.attributes.name;
            refsOutputs(i) = strtod(node.attributes.valueReference);
            select node.children.name
            case "Boolean" then
                typesOutputs(i) = 3; //'boolean'
            case "Enumeration" then
                typesOutputs(i) = 5; //'enumeration'
            case "Float64" then
                typesOutputs(i) = 1; //'real'
            case "Int32" then
                typesOutputs(i) = 2; //'integer'
            case "Integer" then
                typesOutputs(i) = 2; //'integer'
            case "Real" then
                typesOutputs(i) = 1; //'real'
            case "String" then
                typesOutputs(i) = 4; //'string'
            end
            select node.attributes("alias")
            case "alias" then
                // valueReference are the same, do not care
                modifierOutputs(i) = 0;
            case "negatedAlias" then
                modifierOutputs(i) = 1;
            end
            select node.attributes("variability")
            case "discrete" then
                modifierOutputs(i) = modifierOutputs(i) + 2;
            end
        end
        model.out = ones(typesOutputs);
        model.out2 = ones(typesOutputs);
        // set string maximum length
        for i = typesOutputs(typesOutputs == 4)'
            model.out2(i) = stringLength;
        end
        model.outtyp = [1 integerToDouble booleanToDouble 5 enumToDouble](typesOutputs);
        opar(4) = [refsOutputs ; modifierOutputs]; 
        opar(5) = int32(typesOutputs);

        // continuous states
        if fmuImpl == "me 1.0" then
            // the default are "continuous" and "internal"
            query = "//ScalarVariable[" + ...
                "(@variability=""continuous"" or not(@variability))" + ...
                " and " + ...
                "(@causality=""internal"" or not(@causality))" + ...
                "]";
            xmlStates = xmlXPath(xml, query);
            state = zeros(xmlStates.size, 1);

            numberOfContinuousStates = xmlXPath(xml, "//@numberOfContinuousStates");
            opar(14) = uint32(strtod(numberOfContinuousStates(1).content));
            state(opar(14)) = 0; // grow the states if some variables are not declared
        else
            // in FMU 2.0 and 3.0 unknown have a unique derivative index
            if fmuImpl == "me 2.0" then
                derivative = xmlXPath(xml, "//ScalarVariable/Real[@derivative]");
                derivativeIndex = xmlXPath(xml, "//ScalarVariable/Real/@derivative").content;
            elseif fmuImpl == "me 3.0" then
                derivative = xmlXPath(xml, "//ModelVariables/Float64[@derivative]");
                derivativeIndex = xmlXPath(xml, "//ModelVariables/Float64/@derivative").content;
            else
                derivative = list();
                derivativeIndex = [];
            end
            [_, _, ku, _] = unique(strtod(derivativeIndex));

            numberOfContinuousStates = size(ku, "*");
            state = zeros(numberOfContinuousStates, 1);
            opar(14) = uint32(numberOfContinuousStates);

            // fill default values, the last start value win
            for i=1:length(derivative)
                node = derivative(i);

                x0 = strtod(node.attributes.start);
                if x0 <> [] then
                    state(ku(i)) = x0;
                end
            end
        end

        // define the block interface
        if state <> [] then
            model.state = state;
            model.dep_ut = [%f,%t];
        else
            model.dep_ut = [%t,%t];
            if xmlXPath(xml, "//DirectDependency").size == xmlOutputs.size then
                model.dep_ut = [%t,%f];
            end
        end
        
        model.rpar = rpar;
        model.ipar = ipar;
        model.opar = opar;

        if part(fmuImpl, 1:2) == "me" then
            numberOfEventIndicators = xmlXPath(xml, "//@numberOfEventIndicators");
            if length(numberOfEventIndicators) > 0 then
                // number of event indicators in v1 or v2
                model.nzcross = strtod(numberOfEventIndicators.content);
            else
                // EventIndicator in the structure in v3
                numberOfEventIndicators = xmlXPath(xml, "/fmiModelDescription/ModelStructure/EventIndicator");
                model.nzcross = length(numberOfEventIndicators);
            end
        else
            model.nzcross = 0;
        end
        x.model = model;
        
        x.graphics.style = ["blockWithLabel;displayedLabel="+modelName];
        x.graphics.pin = zeros(model.in);
        x.graphics.in_label = labelInputs;
        x.graphics.pout = zeros(model.out);
        x.graphics.out_label = labelOutputs;
        x.graphics.exprs = exprs;
        
        xmlDelete(xml);

    case 'define' then
        exprs = "";
        model=scicos_model();
        model.sim=list("fmu_simulate_me",4);
        model.opar = list();
        model.rpar=[];
        model.ipar = [];
        model.out = [];
        model.in = [];
        model.state = [];
        model.dstate = [];
        model.evtin = [];
        label = "FMU file not defined";
        x = standard_define([9 5],model,exprs,[]);
        x.graphics.style = ["blockWithLabel;displayedLabel="+label];
    end
endfunction

// helper function to resole a FMU filepath
function filepath = resolve_fmu_filepath(fname)
    // if the file does not exist, resolve to :
    //  - PWD/resources for local files
    //  - TMPDIR/resources for SSP support
    //  - fmigetPath() for toolbox demos

    filepath = fname;
    if ~isfile(filepath) then
        filepath = fullfile(PWD, "resources", fname);
    end
    if ~isfile(filepath) then
        filepath = fullfile(TMPDIR, "resources", fname);
    end
    if ~isfile(filepath) then
        filepath = fullfile(fmigetPath(), fname);
    end
    if ~isfile(filepath) then
        msg = gettext("SimpleFMU error: Unable to load ""%s""");
        messagebox(msprintf(msg, fname), "modal", "error");
        filepath = "";
    end
endfunction

// helper function to load an FMU
function [xml, exprs, opar, rpar, ipar, fmuImpl] = load_fmu(filepath, exprs, workdir, fmuImpl)
    xml = [];
    opar = list();
    rpar = [];
    ipar = [];

    if filepath == "" then
        return
    end

    // make the path relative to PWD
    if strindex(filepath, PWD + filesep()) == 1 then
        filepath = part(filepath, (length(PWD)+2):$);
    end

    [path, file_name, extension] = fileparts(filepath);
    if getscilabmode() == "NWNI"
        disp('Loading of '+file_name)
    else
        winId = progressionbar('Loading of '+file_name);
    end
    try
        [workdir, xml, libname, modelIdentifier, fmuImpl] = loadFMU(filepath, workdir, fmuImpl);
        
        // sync some properties to make them accessible at simulation time
        exprs(1) = filepath;
        exprs(2) = workdir;
        exprs(3) = fmuImpl($); // FMI implementation
        if size(exprs, '*') < 9 then
            exprs(4) = "%f"; // debug logging expr
            exprs(5) = "0"; // relative tolerance expr, not set
            exprs(6) = "%t"; // map integers to double
            exprs(7) = "%t"; // map boolean to double
            exprs(8) = "24"; // string max length
            exprs(9) = "%t"; // map enumeration to double
        end
        // following exprs are sett'ed value references, empty by default
        fmiVersion = xmlXPath(xml, "/fmiModelDescription/@fmiVersion")(1).content;

        opar(1) = int8([ascii(fullpath(libname)) 0]);
        opar(2) = []; // inputs references + negated alias
        opar(3) = []; // inputs types
        opar(4) = []; // outputs references + negated alias
        opar(5) = []; // outputs types
        opar(6) = []; // Real references + negated alias
        opar(7) = []; // Integer references + negated alias
        opar(8) = []; // Boolean references + negated aliasex
        opar(9) = []; // Boolean values
        opar(10) = int8([ascii("file:///" + strsubst(fullfile(fullpath(workdir), "resources"), filesep(), "/")) 0]); // path to library
        opar(11) = int8([ascii(xmlXPath(xml, "//@modelName")(1).content) 0]);  // model name
        if fmiVersion == "3.0" then
            opar(12) = int8([ascii(xmlXPath(xml, "//@instantiationToken")(1).content) 0]); // model instantiationToken
        else
            opar(12) = int8([ascii(xmlXPath(xml, "//@guid")(1).content) 0]); // model guid
        end
        opar(13) = int8(0); // debug logging
        opar(14) = [] // number of states. Computed in FMI 2.0 or uint32(xml.root.attributes.numberOfContinuousStates);
        numberOfEventIndicators = xmlXPath(xml, "//@numberOfEventIndicators");
        if length(numberOfEventIndicators) > 0 then
            // number of event indicators in v1 or v2
            opar(15) = uint32(strtod(numberOfEventIndicators(1).content));
        else
            // EventIndicator in the structure in v3
            opar(15) = uint32(length(xmlXPath(xml, "/fmiModelDescription/ModelStructure/EventIndicator")));
        end
        opar(16) = 0.; // relative tolerance not set
    catch
        [str,n]=lasterror();
        msg = ["FMU model hasn''t been loaded." ;
                msprintf("Error %d : ""%s""", n, str)];
        if getscilabmode() == "NWNI"
            disp(msg)
        else
            messagebox(msg,"modal","error");
            close(winId);
            return;
        end
    end
    if getscilabmode() <> "NWNI"
        close(winId);
    end
endfunction
